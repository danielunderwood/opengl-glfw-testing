#include <tutorials.h>

// Access the window from outside (Should be avoided)
extern GLFWwindow * window;

using namespace glm;

glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;

glm::mat4 getViewMatrix(){return ViewMatrix;}
glm::mat4 getProjectionMatrix(){return ViewMatrix;}

// Initial Position -- on +z
glm::vec3 position = glm::vec3(0, 0, 5);
// Initial horizontal angle -- towards -z
float horizontalAngle = 3.14f;
// Initial vertical angle -- none
float verticalAngle = 0.0f;
// Initial field of view -- 45 degrees
float initialFoV = 45.0f;

// Move speed
float speed = 3.0f;
float mouseSpeed = 0.005f;

void computeMatricesFromInputs()
{
  // Get time first time function is called
  static double lastTime = glfwGetTime();

  // Compute dt
  double currentTime = glfwGetTime();
  float deltaTime = float(currentTime - lastTime);

  // Get mouse position
  double xpos, ypos;
  glfwGetCursorPos(window, &xpos, &ypos);

  // Reset mouse position -- should eventuall change to
  //  automatically get window size
  glfwSetCursorPos(window, 1024/2, 768/2);

  // Compute new orientation
  horizontalAngle += mouseSpeed * float(1024/2 - xpos);
  verticalAngle += mouseSpeed * float(768/2 - ypos);

  // Convert from spherical to cartesian
  glm::vec3 direction(
          cos(verticalAngle) * sin(horizontalAngle),
          sin(verticalAngle),
          cos(verticalAngle) * cos(horizontalAngle)
          );

  // Right vector
  glm::vec3 right = glm::vec3(
          sin(horizontalAngle - 3.14f/2.0f),
          0,
          cos(horizontalAngle - 3.14f/2.0f)
  );

  // Up vector
  glm::vec3 up = glm::cross(right, direction);

  // Move forward
  if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
  {
    position += direction * deltaTime * speed;
  }
  // Backwards
  if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
  {
    position -= direction * deltaTime * speed;
  }
  // Strafe right
  if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
  {
    position += right * deltaTime * speed;
  }
  if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
  {
    position -= right * deltaTime * speed;
  }

  // Event callback now needed to use scroll wheel
  float FoV = initialFoV;

  // Set projection matrix
  ProjectionMatrix = glm::perspective(FoV, 4.0f/3.0f, 0.1f, 100.0f);

  // Set camera matrix
  ViewMatrix = glm::lookAt(position, position + direction, up);

  // Update time
  lastTime = currentTime;
}
