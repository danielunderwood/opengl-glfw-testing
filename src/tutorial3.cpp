#include <tutorials.h>

int tutorial3()
{
    // Initialize GLFW
    if(!glfwInit())
    {
        fprintf(stderr, "Failed to initialize GLFW\n");
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // OpenGL version 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1); // Needs to be 2.1 for mac
    // Don't use this for osx
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Set error callback
    glfwSetErrorCallback(errorCallback);
    
    // Open window and create OpenGL context
    GLFWwindow * window;
    window = glfwCreateWindow(1024, 768, "OpenGL Testing", NULL, NULL);
    if(window == NULL)
    {
        fprintf(stderr, "Failed to open GLFW Window. Intel GPU Not Compatible with OpenGL 3.3\n");
        glfwTerminate();
        return -1;
    }

    // Should this go after the next block?
    glfwMakeContextCurrent(window);



    // Initialize GLEW
    glewExperimental = true;
    if(glewInit() != GLEW_OK)
    {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    // Setup input
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    // Set background color (dark blue)
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

    // Make Vertex Array object and set as current
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);



    // Create and compile GLSL program from shaders
    GLuint programID = loadShaders("shaders/tutorial3vertex.glsl", "shaders/SimpleFragmentShader.fragmentshader");

    // Get handle for MVP uniform
    GLuint MatrixID = glGetUniformLocation(programID, "MVP");

    // Projection matrix: 45 degree FOV, 4:3 ratio, Display Rangle [0.1 unit, 100 unit]
    glm::mat4 Projection = glm::perspective(45.0f, 4.0f/3.0f, 0.1f, 100.0f);

    // Camera matrix
    glm::mat4 View = glm::lookAt(
                glm::vec3(4, 3, 3), // Camera at (4, 3, 3)
                glm::vec3(0, 0, 0), // Looks at origin
                glm::vec3(0, 1, 0) // +y up
                );

    // Model matrix (Identity since model at origin, but will change for each model)
    glm::mat4 Model = glm::mat4(1.0f);

    // ModelViewProjection (MVP) Matrix
    glm::mat4 MVP = Projection * View * Model;

    // Points of triangle
    static const GLfloat g_vertex_buffer_data[] =
    {
      -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        0.0f, 1.0f, 0.0f
    };

    // Buffer to pass triangle to OpenGL
    // Identifies buffer
    GLuint vertexbuffer;

    // Generate buffer and put identifier in vertexbuffer
    glGenBuffers(1, &vertexbuffer);

    // Bind the buffer
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

    // Give vertices to OpenGL
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    do
    {
        // Clear screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Use shader
        glUseProgram(programID);

        // Send transformation to currently bound shader in MVP Uniform
        // Needs to be done for each model rendered since MVP different
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);


        // 1st attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
            0,			// Attribute 0. No reason, but must match value in shader
            3,			// Size
            GL_FLOAT,	// Type
            GL_FALSE,	// Normalized?
            0,			// Stride
            (void*)0	// Array buffer offset
            );

        // Draw Triangle
        glDrawArrays(GL_TRIANGLES, 0, 3); // Starting from vertex 0 with 3 total vertices (1 triangle)

        glDisableVertexAttribArray(0);

        // Swap buffers

        glfwSwapBuffers(window);
        glfwPollEvents();
    } while(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);

    return 0;
}
