#include <tutorials.h>

GLuint loadBMP(const char * imagePath)
{
    // Data read from the header of BMP file
    unsigned char header[54]; // BMP files have 54-byte headers
    unsigned int dataPos; // Where data begins
    unsigned int width, height;
    unsigned int imageSize; // width*height*3
    
    // Actual RGB data
    unsigned char * data;
    
    // Open File
    FILE * file = fopen(imagePath, "rb");
    if(!file)
    {
        printf("Image could not be opened\n");
        return 0;
    }
    
    // Error if header is not 54 bytes (BMP File)
    if(fread(header, 1, 54, file) != 54)
    {
        printf("Not a correct BMP file\n");
        return false;
    }
    
    // Check that first two bytes are 'B' and 'M'
    if(header[0] != 'B' || header[1] != 'M')
    {
        printf("Not a correct BMP file\n");
        return 0;
    }
    
    // Read size of image, location of data in file, etc.
    dataPos = *(int*)&(header[0x0A]);
    imageSize = *(int*)&(header[0x22]);
    width = *(int*)&(header[0x12]);
    height = *(int*)&(header[0x16]);
    
    // Guess missing information if file is misformatted
    if(imageSize == 0) imageSize = width * height * 3; // One each for RGB
    if(dataPos == 0) dataPos = 54; // Header should be done here
    
    // Now that image size is known, allocate memory to load image into and read
    data = new unsigned char[imageSize];
    fread(data, 1, imageSize, file);
    
    // Close file
    fclose(file);
    
    // Create OpenGL texture
    GLuint textureID;
    glGenTextures(1, &textureID);
    
    // Bind newly created texture -- All future texture functions will modify this
    glBindTexture(GL_TEXTURE_2D, textureID);
    
    // Give image to OpenGL
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR                    );

    // Generate mipmaps
    glGenerateMipmap(GL_TEXTURE_2D);
    
}
