#include <tutorials.h>

// Everything removed since GLFW no longer loads images
GLuint loadTGA(const char *imagePath)
{
   /* // Create OpenGL texture
    GLuint textureID;
    glGenTextures(1, &textureID);

    // Bind new texture to texture functions
    glBindTexture(GL_TEXTURE_2D, textureID);

    // Will no longer work as the following function has been removed!
    glfwLoadTexture2D(imagePath, 0);

    // Trilinear filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

    // Return id of texture */
    return -1;
}
