#version 330 core

// Interpolated values from vertex shader
in vec3 fragmentColor;

// Copy to final output color
out vec3 color;

void main()
{
	// Output color is color specified in vertex shader,
	//	interpolated by 3 surrounding vertices
	color = fragmentColor;
}