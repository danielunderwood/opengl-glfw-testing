#version 330 core

// Interpolated values from vertex shaders
in vec2 UV;

// Output data
out vec3 color;

// Values that stay constant for whole mesh
uniform sampler2D myTextureSampler;

void main()
{
	// Output color -- Color of texture at specified UV coord
	color = texture(myTextureSampler, UV).rgb;
}