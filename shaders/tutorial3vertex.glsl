#version 330 core
in vec3 vertexPosition_modelspace;
uniform mat4 MVP;

void main()
{
	// Output position of vertec, in clip space : MVP * position
	vec4 v = vec4(vertexPosition_modelspace, 1); // Transform homogeneous 4d position vector
	gl_Position = MVP * v;
}