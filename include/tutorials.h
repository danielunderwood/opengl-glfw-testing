#pragma once

#include <stdio.h>
#include <stdlib.h>

#define GLEW_STATIC
#include <GL/glew.h>
#include <glfw3.h>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>

#include <controls.h>

int tutorial1();
int tutorial2();
int tutorial3();
int tutorial4();
int tutorial5();
int tutorial6();

void errorCallback(int error, const char * description);

GLuint loadShaders(const char * vertex_file_path, const char * fragment_file_path);
GLuint loadBMP(const char * imagePath);
GLuint loadTGA(const char * imagePath); // No longer works!
GLuint loadDDS(const char * imagePath);
